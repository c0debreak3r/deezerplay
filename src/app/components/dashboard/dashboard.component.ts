import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  private mocksMusic = [];
  private searchMusics = [];
  private audio = new Audio();
  private currentSong;
  private recommended = [];

  constructor(private http: HttpService) { }

  public getDuration(duration: number) : string {
    let minutes = Math.floor(duration / 60);
    let seconds = duration - minutes * 60;
    let ret = minutes + ':';
    if (seconds < 10)
      ret += '0' + seconds;
    else
      ret += seconds;

    return ret;
  }

  ngOnInit() {
    /*this.http.get('http://api.deezer.com/track/100001884', {}).then((resp) => {

      this.mocksMusic.push(resp);
      console.log(resp);
    });
    this.http.get('http://api.deezer.com/track/100001812', {}).then((resp) => {
      this.mocksMusic.push(resp);
      console.log(resp);
    });
    this.http.get('http://api.deezer.com/track/100001800', {}).then((resp) => {
      this.mocksMusic.push(resp);
      console.log(resp);
    });
    this.http.get('http://api.deezer.com/track/100001884', {}).then((resp) => {
      this.mocksMusic.push(resp);
      console.log(resp);
    });
    this.http.get('http://api.deezer.com/track/100001712', {}).then((resp) => {
      this.mocksMusic.push(resp);
      console.log(resp);
    });
    this.http.get('http://api.deezer.com/track/100001600', {}).then((resp) => {
      this.mocksMusic.push(resp);
      console.log(resp);
    });
    this.http.get('http://api.deezer.com/track/100001874', {}).then((resp) => {
      this.mocksMusic.push(resp);
      console.log(resp);
    });
    this.http.get('http://api.deezer.com/track/100001810', {}).then((resp) => {
      this.mocksMusic.push(resp);
      console.log(resp);
    });*/
    
  }

  private searchTracks(name) {
    this.searchMusics.length = 0;
    this.http.get('http://localhost:3000/searchTracks?name=' + name, {}).then((resp) => {
      console.log(resp);
      for (let val in resp['title']) {
      let new_str = resp['artist'][val].replace(/u'/g, '"').replace(/':/g, '":').replace(/',/g, '",');
      const final_str = new_str.substring(0, new_str.indexOf((', "radio"'))) + '}';
      console.log(final_str);
      //console.log(resp['artist'][val].replace(/u'/g, '"').replace(/':/g, '":').replace(/',/g, '",'));        
      const artist = JSON.parse(final_str);        
        this.searchMusics.push({
          'id': val,
          'title': resp['title'][val],
          'preview': resp['preview'][val],
          'duration': resp['duration'][val],
          "artist": {name: artist['name']}
        });
      }
      console.log(this.searchMusics);
    }).catch(err => console.log(err));
  }

  private testStart() {
    this.http.get('http://localhost:3000/start', {}).then((resp) => {
      console.log(resp);
    });
  }

  private getPlaylist() {
    if (this.mocksMusic.length <= 0)
      return ;
    let seeds = [];
    this.recommended.length = 0;
    for (let track in this.mocksMusic) {
      seeds.push(parseInt(this.mocksMusic[track]['id']));
    }
    console.log(seeds);
    this.http.postOptions('http://localhost:3000/playlist', {"seeds": seeds }, {responseType: 'json'}).then((resp) => {
      console.log(resp);
      for (let val in resp['title']) {
        this.recommended.push({
          'title': resp['title'][val],
          'preview': resp['preview'][val],
          'artist': resp['artist'][val]
        });
      }
      /*let new_str = resp.toString().replace(/u'/g, '"').replace(/':/g, '":').replace(/',/g, '",').replace(/True/g, '"True"').replace(/False/g, '"False"').replace(/NaN/g, '"NaN"');
      
      //{"picture"
      console.log(new_str);
      console.log(JSON.parse(new_str));*/
    }).catch(err => console.log(err));
  }

  private keyPress(event) {
    console.log(event);
    if (event.length >= 3)
      this.searchTracks(event);
    else
      this.searchMusics.length = 0;

  }

  private playMusic(music, fromSearch){
    this.currentSong = music;
    this.audio.src = music.preview;
    this.audio.load();
    this.audio.play();
    if (fromSearch != null && fromSearch == 0) {
      this.mocksMusic.push(music);
      this.searchMusics.length = 0;    
      this.getPlaylist();  
    }
  }

  private deleteFromPlaylist(music) {
    const index = this.mocksMusic.indexOf(music);
    if (index > -1) {
      this.mocksMusic.splice(index, 1);
      this.getPlaylist();
    }
  }

  private nextOrPrev(command) {
    const index = this.mocksMusic.indexOf(this.currentSong);    
    if (command == 0) {
      if (index == -1) {
        const rec_index = this.recommended.indexOf(this.currentSong)
        if (rec_index != -1) {
          if (rec_index != 0)
            this.playMusic(this.recommended[rec_index - 1], 1);
          else 
            this.playMusic(this.recommended[this.recommended.length - 1], 1);
        }
        return;
      }
      if (index == 0) {
        return ;
      } else {
        this.playMusic(this.mocksMusic[index - 1], 1);
      }
    }
    else {
      if (index == -1) {
        const rec_index = this.recommended.indexOf(this.currentSong)
        if (rec_index != -1) {
          if (rec_index != this.recommended.length - 1)
            this.playMusic(this.recommended[rec_index + 1], 1);
          else 
            this.playMusic(this.recommended[0], 1);
        }
        return;
      }
      if (index == this.mocksMusic.length - 1) {
        this.playMusic(this.mocksMusic[0], 1);
      } else {
        this.playMusic(this.mocksMusic[index + 1], 1);
      }
    }
  }
}
